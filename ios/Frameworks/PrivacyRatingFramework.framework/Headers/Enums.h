//
//  Enums.h
//  PrivacyRatingFramework
//
//  Created by Yossi Rating on 21/05/2019.
//  Copyright © 2019 PrivacyRating. All rights reserved.
//

#ifndef Enums_h
#define Enums_h

#import "Defines.h"

typedef NS_ENUM(NSInteger, EventType) {
    Unknown = 4,
    DataViolation = 5,
    AllowedByPolicy = 6,
    BlockedByPolicy = 7,
    StructureViolation = 8,
    Mapping = 9,
    InWhiteList = 50,
    InSoftLaunch = 51,
    InKillSwitch = 52
};

typedef NS_ENUM(NSInteger, LevelPolicy) {
    ChannelArray = -1,
    Channel = 0,
    Domain = 1,
    Endpoint = 2,
    Param = 3
};

typedef NS_ENUM(NSInteger, SdkMode) {
    SdkMapping = 0,
    SoftLaunch = 1,
    Enforce = 2,
    KillSwitch = 3,
    Testing
};

typedef NS_ENUM(NSInteger, Channels) {
    ChannelHttp = 0,
    ChannelIFrame = 2,
    ChannelTcp = 3,
    ChannelUdp = 4,
    ChannelStorage = 5,
    ChannelBlueTooth = 6,
    ChannelNfc = 7
};

typedef NS_ENUM(NSInteger, RegexDataViolation) {
    RegexDataViolationNone = 0,
    RegexVisa = 1,
    RegexMastercard = 2,
    RegexIsraelId = 3,
    RegexHacker = 4
}; // error DATA_VIOLATION // non-structurl // EP

typedef NS_ENUM(NSInteger, RegexStructuralViolation) {
    RegexStructuralViolationNone = 0,
    RegexNumbersOnly = 1,
    RegexTextOnly = 2,
    RegexLessThan25 = 3
};// error PARAM_STRUCTURE_VIOLATION // structurl // PARAM

typedef NS_ENUM(NSInteger, SplitDataParseType) {
    splitNone = 0,
    splitUrl,
    splitJsonDictionary,
    splitJsonArray,
    splitQuery,
    splitEncoding,
    splitExtraCharactersDictionary,
    splitExtraCharactersArray,
    splitSeperatedByCommaArray,
    splitBrackets,
    //    splitSimpleDictionary,
    //    splitSimpleArray,
    splitSimplekey
};

typedef NS_ENUM(NSInteger, MessagingEvent) {
    MessagingEventNone = 0,
    MessagingEventFilteredParameter,
    MessagingEventBlockedParameter,
    MessagingEventAllowedParameter,
    MessagingEventAnonymousParameter,
    MessagingEventPsudoParameter,
    MessagingEventMappingParameter,
    MessagingEventSoftLaunchParameter,
    MessagingEventBlockedUrlNotInPolicy,
    MessagingEventBlockedUrl,
    MessagingEventBlockedEndpointByPolicy,
    MessagingEventDownloadedPolicy,
    MessagingEventPeriodicalDownloadPolicy,
    MessagingEventResetPolicy,
    MessagingEventUploadReport,
    MessagingEventIsMapping,
    MessagingEventIsSafeMode,
    MessagingEventDataViolation,
    MessagingEventParamStructuralViolation
};

typedef NS_ENUM(NSInteger, SplitInflateType) {
    SplitInflateTypeDictionary,
    SplitInflateTypeArray,
    SplitInflateTypeStringArray,
    SplitInflateTypeStringQuery,
    SplitInflateTypeStringDictionary,
    SplitInflateTypeStringObject,
    SplitInflateTypeStringNull,
    SplitInflateTypeObject
};


typedef NS_ENUM(NSInteger, EnforceType) {
    EnforceTypeAllowed = 0,
    EnforceTypeBlocked = 1,
    EnforceTypeAnonymous = 2,
    EnforceTypePsudo = 3
};


#endif /* Enums_h */
