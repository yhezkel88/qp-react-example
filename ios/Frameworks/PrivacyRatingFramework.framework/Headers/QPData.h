//
//  QPData.h
//  PrivacyRatingFramework
//
//  Created by Yossi Rating on 26/12/2018.
//  Copyright © 2018 PrivacyRating. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QPData : NSObject

+ (NSDictionary *_Nullable) split:(NSString *) dataString;
+ (NSDictionary *_Nullable) splitData:(NSString *) dataString;
+ (NSString *_Nullable) compose:(NSDictionary *) dictionaryData;
+ (NSArray *) flaMap:(NSString *) dataString;
+ (id _Nullable) composeJsonArray:(NSDictionary *) jsonDictionary;
+ (id _Nullable) composeJsonDictionary:(NSDictionary *) jsonDictionary;

@end

NS_ASSUME_NONNULL_END
