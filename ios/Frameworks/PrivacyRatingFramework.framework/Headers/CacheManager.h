//
//  CacheManager.h
//  PrivacyRatingFramework
//
//  Created by Yossi Rating on 12/02/2019.
//  Copyright © 2019 PrivacyRating. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CacheManager : NSObject

+ (CacheManager *)sharedManager;
- (void) addObservedObjectForTherad:(id)object
                          sourceString:(NSString *)sourceString;
- (id _Nullable) observedObjectForUrl:(NSURL *)url
                            sourceString:(NSString *)sourceString;
- (NSURL *_Nullable) urlByThreadInfo:(NSString *)sourceString;
- (void) readUrlDictionary;
- (void) writeUrlDictionary:(BOOL)stopMaintainenceFlag;
- (void) startMaintainence;
- (void) clearAll;

@end

NS_ASSUME_NONNULL_END
