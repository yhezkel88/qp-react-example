//
//  TestManager.h
//  PrivacyRatingFramework
//
//  Created by Yossi Rating on 27/01/2019.
//  Copyright © 2019 PrivacyRating. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Defines.h"
#import "Enums.h"
//#import "BaseReportingProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@class OrderedDictionary;
@class BaseReporting;

@interface TestManager : NSObject
+ (TestManager *)sharedManager;
- (BOOL) testRegexIsParamStructureVialation:(NSInteger) regexId forValue:(NSString *)value;
- (BOOL) testRegexIsDataViolation:(NSInteger) regexId forValue:(NSString *)value;
- (BOOL) testRegexIsParamStructureVialation:(NSData *)policyJsonData forRequest:(NSData *)requestData
                       parameterKeyFiltered:(NSString *)paramKey
                     parameterValueFiltered:(NSString *)paramValue
                                        url:(NSURL *)url;
- (BOOL) testRegexIsDataViolation:(NSData *)policyJsonData forRequest:(NSData *)requestData
             parameterKeyFiltered:(NSString *)paramKey
           parameterValueFiltered:(NSString *)paramValue
                              url:(NSURL *)url;
- (BOOL) testFilteredParameter:(NSData *)policyJsonData forRequest:(NSData *)requestData
          parameterKeyFiltered:(NSString *)paramKey
        parameterValueFiltered:(NSString *)paramValue
                           url:(NSURL *)url;
- (BOOL) testRegexIsParamStructureVialation:(NSData *)policyJsonData forRequest:(NSData *)requestData url:(NSURL *)url;
- (BOOL) testRegexIsDataViolation:(NSData *)policyJsonData forRequest:(NSData *)requestData url:(NSURL *)url;
- (BOOL) testFilteredParameter:(NSData *)policyJsonData forRequest:(NSData *)requestData url:(NSURL *)url;
- (BOOL) testHash:(NSString *) original expectedString:(NSString *)expectedString;
- (BOOL) testHasDomainForUrl:(NSURL *)url policyJsonData:(NSData *)policyJsonData;
- (BOOL) testHasEndPointForUrl:(NSURL *)url policyJsonData:(NSData *)policyJsonData;
- (BOOL) inWhiteList:(NSString *)urlString policyJsonData:(NSData *)policyJsonData;
- (BOOL) testDomainBlockedForUrl:(NSURL *)url policyJsonData:(NSData *)policyJsonData;
- (BOOL) testEndPointBlockedForUrl:(NSURL *)url policyJsonData:(NSData *)policyJsonData;
- (BOOL) testEndPointHasParameterKey:(NSString *)parameterKey
                           isBlocked:(BOOL)isBlocked
                              forUrl:(NSURL *)url
                      policyJsonData:(NSData *)policyJsonData;
- (BOOL) testEndPointHasRegex:(NSString *_Nullable)parameterKey
                  policyLevel:(NSInteger)policyLevel
                  numberArray:(NSArray *)numberArray
                       forUrl:(NSURL *)url
               policyJsonData:(NSData *)policyJsonData;

- (NSURL *) testFilterUrl:(NSURL *) url
                 withData:(NSData *_Nullable)data
              forReportId:(NSString *)reportIdentifier;

- (BOOL) testFilterForUrl:(NSURL *) url;
- (BOOL) testFilterForUrl:(NSURL *) url
         allowedParameter:(NSString *)allowedParameter
        filteredParameter:(NSString *)filteredParameter
         blockedParameter:(NSString *)blockedParameter;

- (BOOL) testYoniWasReport:(NSString *) paramKey
                forRequest:(NSURLRequest *)request;
- (BOOL) testRealFacebookCustomEventRequest:(NSString *)parameterKey
                       valueStringToCompare:(NSString *)valueStringToCompare
                           validationString:(NSString *)validationString;

- (BOOL) testHasReport:(NSString *) paramKey
            paramValue:(NSString *)paramValue
                andUrl:(NSURL *) url;

- (void) setReportingArray:(NSArray *)domainReportings;
- (void) resetReportingArray;


- (NSDictionary *_Nullable) testFilterProtobuf:(NSData *) data url:(NSURL *)url forReportId:(NSString *) reportIdentifier;
- (NSDictionary *_Nullable) testBlockedFilterProtobuf:(NSData *) data url:(NSURL *)url forReportId:(NSString *) reportIdentifier;
- (NSDictionary *_Nullable) testAllowedProtobufParams:(NSData *) data url:(NSURL *)url forReportId:(NSString *) reportIdentifier;
- (NSDictionary *_Nullable) testAllProtobufParams:(NSData *) data;
- (BOOL) testNewParseInflate:(NSString *) jsonString;

- (BOOL) testFilterUrlInPolicyByRequest:(NSURLRequest *) request
                            filteredKey:(NSString *_Nullable) filteredKey
                          filteredValue:(NSString *_Nullable) filteredValue
             regexStructureVialationKey:(NSString *_Nullable) regexStructureVialationKey
           regexStructureVialationValue:(NSString *_Nullable) regexStructureVialationValue
                  regexDataViolationKey:(NSString *_Nullable) regexDataViolationKey
                regexDataViolationValue:(NSString *_Nullable) regexDataViolationValue;

- (NSDictionary *_Nullable) protobufDataToDictionary:(NSData *)data;
- (NSData *_Nullable) protobufDictionaryToData:(NSDictionary *)dictionary;
- (NSArray * _Nullable) testProtobufFlatmapAndInflater:(NSData *) httpBody forUrl:(NSURL *)url;
- (void) testClearReporting;
- (id _Nullable) allowedParameterByKey:(NSString *_Nonnull)keySearch
                                forUrl:(NSURL *_Nonnull) url
                            forChannel:(Channels)channel;
- (id _Nullable) violationParameterByKey:(NSString *_Nonnull)keySearch
                                  forUrl:(NSURL *_Nonnull) url
                              forChannel:(Channels)channel ;

- (void) testReportingMappingWithUrl:(NSURL *)url
                       andDictionary:(NSDictionary *) objectDictionary
                          forChannel:(Channels) channel;

- (void) testReportingForMappingWithUrl:(NSURL *_Nonnull)url
                             andBody:(NSData * _Nullable)httpBody
                             forChannel:(Channels) channel;
- (void) testReportingViolatingWithUrlOnly:(NSURL *)url
                                forChannel:(Channels)channel;
- (void) testReportingBlockedWithUrlOnly:(NSURL *)url
                              forChannel:(Channels)channel;

- (void) reportingForDomain:(NSString *) domain
                   endPoint:(NSString *_Nullable)endPoint
              allowedParams:(NSDictionary *_Nullable)allowedParams
            violationParams:(NSDictionary *_Nullable)violationParams
              blockedParams:(NSDictionary *_Nullable)blockedParams
   regexNonStructuralParams:(NSDictionary *_Nullable)regexNonStructuralParams
      regexStructuralParams:(NSDictionary *_Nullable)regexStructuralParams
                      state:(EventType)state;

- (void) reportingAllowedForDomain:(NSString *) domain
                          endPoint:(NSString *_Nullable)endPoint
                 allowedParameters:(NSDictionary *)allowedParameters;
//
//- (void) reportingForDomain:(NSString *) domain
//                   endPoint:(NSString *_Nullable)endPoint
//         violatedParameters:(NSDictionary *_Nullable)violatedParameters
//          blockedParameters:(NSDictionary *_Nullable)blockedParameters
//    endPointRegexParameters:(NSDictionary *_Nullable)endPointRegexParameters
//       paramRegexParameters:(NSDictionary *_Nullable)paramRegexParameters
// domainViolationNotInPolicy:(BOOL)domainViolationNotInPolicy
//     domainViolationBlocked:(BOOL)domainViolationBlocked
//endPointViolationNotInPolicy:(BOOL)endPointViolationNotInPolicy
//   endPointViolationBlocked:(BOOL)endPointViolationBlocked;

- (void) readReportingFromFile;

- (void) testAddUnknown:(NSDictionary *)parameters
                    url:(NSURL *)url;
- (void) testAddNoProblemAllowed:(NSDictionary *)parameters
                             url:(NSURL *)url;
- (void) testAddNoProblemBlocked:(NSDictionary *)parameters
                             url:(NSURL *)url;
- (void) testRegexAddRegexDataViolation:(RegexDataViolation) regexDataViolation
                                    url:(NSURL *)url
                          forParameters:(NSDictionary *)parameters;
- (void) testRegexAddRegexStructueViolation:(RegexStructuralViolation) structureViolation
                                        url:(NSURL *)url
                              forParameters:(NSDictionary *)parameters;
- (void) testAddMapping:(NSDictionary *)parameters
                    url:(NSURL *)url;


- (void) testRemoveAllReportsNotWithUrl:(NSURL *_Nonnull)url;
- (void) testRemoveAllReportsNotWithRequest:(NSURLRequest *)request;
- (void) testReportingRequest:(NSURLRequest *) request
               policyJsonData:(NSData *)policyJsonData
                      version:(NSString *)version
                  removeItems:(BOOL)removeItems
                    isMapping:(BOOL)isMapping;
- (NSInteger) totalParametersCount:(NSURL *_Nonnull) url forChannel:(Channels)channel;
- (NSInteger) allowedParametersCount:(NSURL *) url forChannel:(Channels)channel;
- (NSInteger) violationParametersCount:(NSURL *) url forChannel:(Channels)channel;
- (NSInteger) blockedParametersCount:(NSURL *) url forChannel:(Channels)channel;
- (NSInteger) endPointRegexCount:(NSURL *) url forChannel:(Channels)channel;
- (NSInteger) paramRegexCount:(NSURL *) url forChannel:(Channels)channel;
- (void) clearReportsAndPersistance;
- (void) clearCache;
- (void) setTestDomainName:(NSString *)domainName;
- (NSDictionary *) hashValues:(NSURL *) url;
- (NSString *) hashString:(NSString *)stringToHash;
- (void) testReport:(NSString *)version;
- (NSInteger) testReportCount:(NSInteger)channelInteger;
- (BOOL) testNewFlatmapValueChange:(NSString *) stringData
                         changeKey:(NSString *_Nonnull)changeKey
              expectedInitialValue:(NSObject *_Nonnull)expectedInitialValue
                     expectedValue:(NSObject *_Nonnull) expectedValue;

- (BOOL) testNewFlatmapKeysAndValues:(NSString *) stringData
                        expectedKeys:(NSArray *_Nonnull)expectedKeys
                      expectedValues:(NSArray *_Nonnull) expectedValues;

- (BOOL) testNewFlatmapFlattenInflateEven:(NSString *) stringData;
- (NSObject *_Nullable) testAnonymousObject:(NSObject *_Nonnull)object;
- (NSObject *_Nullable) testPsudoObject:(NSObject *_Nonnull)object;
//- (BOOL) testProtobufAnonymousPsudo:(NSDictionary *)protobuf;
- (void) clearReports;
- (void) clearReportedReports;
- (NSArray *_Nullable) testReports;
- (void) resetReportProblemIndex;
//- (NSData *)gzippedData:(NSData *_Nonnull)data;

@property (strong, nonatomic) NSURL * _Nullable reportingURL;
@property (strong, nonatomic) NSString *_Nullable domainNameForTesting;
@property (nonatomic) BOOL simulatedParameterOnly;

@end

NS_ASSUME_NONNULL_END
