//
//  PrivacyRatingManager.h
//  PrivacyRatingFramework
//
//  Created by Ari Deane on 25/11/2018.
//  Copyright © 2018 PrivacyRating. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Defines.h"
#import "Enums.h"
@class PolicyLevel;
NS_ASSUME_NONNULL_BEGIN



@class ReportingDomain;
@class FilterUrlResult;
@class FilteredObjects;

@interface PrivacyRatingManager : NSObject
+ (PrivacyRatingManager *)sharedManager;
- (void) configure:(NSString *)segment;
- (void) updatePolicy:(BOOL)fromTimer;
- (void) changeSubDomain:(NSString *)newSubDomain
            oldSubDomain:(NSString *)oldSubDomain;
- (void) changeDomain:(NSString *)newDomain
            oldDomain:(NSString *_Nullable)oldDomain;
- (void) startReportingPeriodically;
- (void) startTestReporting;
	
- (BOOL) isMappingEnabled;
- (BOOL) isSoftLaunchEnabled;
- (NSString *) policyVersion;
- (NSString *) policyMode;
- (void) setIsMappingEnabled:(BOOL) enabled;

- (BOOL) isTestingMode;
- (void) setTestingMode;
- (NSInteger) reportingPackageNumberOfItems;
- (void) setReportingPackageNumber:(NSInteger)reportingPackageNumber;
- (void) resetPolicy;
- (void) testSetPolicy:(NSData *) policyData policyName:(NSString *) policyName;
- (BOOL) inWhiteList:(NSURL *)url;
- (void) clearReportsAndPersistance;
- (void) testLocalReportOnly:(NSString *)version;
- (void) testOnReport:(NSString *)version;
- (void) stopReporting;
- (BOOL) hasStopped;
- (void) stopUpdatingPolicy;
- (void) showToast:(NSString *)bodyMessage;

@end


NS_ASSUME_NONNULL_END
