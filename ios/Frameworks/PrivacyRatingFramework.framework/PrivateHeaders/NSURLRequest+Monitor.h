//
//  NSURLRequest+Monitor.h
//  PrivacyRatingFramework
//
//  Created by Ari Deane on 19/11/2018.
//  Copyright © 2018 PrivacyRating. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSURLRequest (Monitor)

@end

NS_ASSUME_NONNULL_END
