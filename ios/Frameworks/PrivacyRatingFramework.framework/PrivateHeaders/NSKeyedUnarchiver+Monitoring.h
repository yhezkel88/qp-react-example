//
//  NSKeyedUnarchiver+Monitoring.h
//  PrivacyRatingFramework
//
//  Created by Yossi Rating on 13/02/2019.
//  Copyright © 2019 PrivacyRating. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSKeyedUnarchiver (Monitoring)

@end

NS_ASSUME_NONNULL_END
