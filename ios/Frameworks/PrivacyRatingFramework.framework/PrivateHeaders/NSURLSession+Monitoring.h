//
//  NSURLSession+Monitoring.h
//  PrivacyRatingFramework
//
//  Created by Ari Deane on 18/11/2018.
//  Copyright © 2018 PrivacyRating. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "Constant.h"

NS_ASSUME_NONNULL_BEGIN

@interface NSURLSession (Monitoring)
+ (void)swizzleFunction:(SEL)defaultSelector
       swizzledSelector:(SEL)swizzledSelector;


@end

NS_ASSUME_NONNULL_END
