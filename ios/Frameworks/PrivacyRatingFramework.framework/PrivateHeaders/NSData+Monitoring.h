//
//  NSData+Monitoring.h
//  PrivacyRatingFramework
//
//  Created by Ari Deane on 20/11/2018.
//  Copyright © 2018 PrivacyRating. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSData (Monitoring)

@end

NS_ASSUME_NONNULL_END
