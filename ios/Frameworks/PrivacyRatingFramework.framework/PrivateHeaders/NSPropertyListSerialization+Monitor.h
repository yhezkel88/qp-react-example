//
//  NSPropertyListSerialization+Monitor.h
//  PrivacyRatingFramework
//
//  Created by Yossi Rating on 11/02/2019.
//  Copyright © 2019 PrivacyRating. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSPropertyListSerialization (Monitor)

@end

NS_ASSUME_NONNULL_END
